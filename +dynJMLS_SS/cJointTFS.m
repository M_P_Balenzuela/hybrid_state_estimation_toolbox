function [cL_c_arr,s_c_arr,r_c_arr,z_c_arr,lnws_arr,mus_arr,cPs_arr,zs_arr] = cJointTFS(cdynJMLS,u,ym,cPf_arr,muf_arr,lnwf_arr,zf_arr,ym_pr,u_pr,  cLc_arr,sc_arr,rc_arr,zc_arr)

nx = size(cdynJMLS.model(1).A,1);
two_nx = 2*nx;
nz = cdynJMLS.N;
lnT = cdynJMLS.lnT;

if nargin < 10 % initialising
   Nb = 1;
   cLb = zeros(nx,nx);
   sb = zeros(nx,1);
   rb = 0;
else
   Nb = length(rc_arr);
end    

    N_b = Nb*nz;
    cL_c_arr = nan(nx,nx,N_b);
    s_c_arr = nan(nx,N_b);
    r_c_arr = nan(N_b,1);
    z_c_arr = nan(N_b,1);
    
    idx = 0;
    for zi = 1: nz % discrete switching
            [A,b,cQ,C,d,cR] = HSET.dynLG_SS.cUnpack(cdynJMLS.model(zi),ym,u);
            d_minus_ym = d - ym;
            sidx = idx;
            for i = 1:Nb
                idx = idx + 1;
                if nargin < 10
                    [cL_c_arr(:,:,idx),s_c_arr(:,idx),r_c_arr(idx)] = HSET.LG.cBIF_Correction(cLb,sb,rb,C,d_minus_ym, cR);
                else
                    rci = rc_arr(i) - 2*lnT(zc_arr(i),zi);
                    [cLb,sb,rb] = HSET.LG.cBIF_BackPropagation(cLc_arr(:,:,i),sc_arr(:,i),rci,A,b,cQ);
                    [cL_c_arr(:,:,idx),s_c_arr(:,idx),r_c_arr(idx)] = HSET.LG.cBIF_Correction(cLb,sb,rb,C,d_minus_ym, cR);
                end
            end
            z_c_arr(sidx+1:idx) = zi;
    end

Nf = length(lnwf_arr);
N_s = N_b*Nf;
cPs_arr = nan(two_nx,two_nx,N_s);
mus_arr = nan(two_nx,N_s);
lnws_arr = nan(N_s,1);
zs_arr = nan(2,N_s);

idx = 0;
zff = -1; % -1 is not an valid model, which forces the model to be loaded for first iteration
for f = 1:Nf
    
    % Don't reload the active model
    if (zff ~= zf_arr(f))
        zff = zf_arr(f);
        [A,b,cQ] = HSET.dynLG_SS.cUnpack(cdynJMLS.model(zff),ym_pr,u_pr);
    end
    
       
    
    cPf = cPf_arr(:,:,f);
    muf = muf_arr(:,f);
    lnwf = lnwf_arr(f);
    sidx = idx;
    for bb = 1:N_b
        idx = idx + 1;
        zbb = z_c_arr(bb);
        cLc = cL_c_arr(:,:,bb);
        sc = s_c_arr(:,bb);
        rc = r_c_arr(bb);
        [cPs_arr(:,:,idx),mus_arr(:,idx),lnws_arr(idx)] = HSET.LG.cTFjoint_former(cPf, muf,lnwf, cLc, sc, rc,A,b,cQ,lnT(zbb,zff));
        zs_arr(2,idx) = zbb;
    end
    zs_arr(1,sidx+1:idx) = zff;
end

 r_c_arr = HSET.misc.normalise_lnw(r_c_arr);
 
%  idx = (r_c_arr == 0);
%  if any(idx)
%      cL_c_arr = cL_c_arr(:,:,idx);
%      s_c_arr = s_c_arr(:,idx);
%      r_c_arr = r_c_arr(idx);
%      z_c_arr = z_c_arr(idx);
%  end
 
lnws_arr = HSET.misc.normalise_lnw(lnws_arr);

