function [xtrue_arr,y_arr] = simulate(x0,SS,u_arr)

% Unpack required variables
A = SS.A;
C = SS.C;
Q = SS.Q;
R = SS.R;

% Grab dims.
[nu,N] = size(u_arr);
[ny,nx] = size(C);

% Unpack optional variables
if isfield(SS,'B')
    B = SS.B;
else
    B = zeros(nx,nu);
end

if isfield(SS,'D')
    D = SS.D;
else
    D = zeros(ny,nu);
end

if isfield(SS,'S')
    S = SS.S;
else
    S = zeros(nx,ny);
end



% Allocate space
xtrue_arr = nan(nx,N+1);
y_arr = nan(ny,N);

% Load initial condition
x = x0;
xtrue_arr(:,1) = x0;

% Setup system noise
joint_noise_cov = [Q S;
                S.' R];

joint_noise_SD = sqrtm(joint_noise_cov);

randnstore = randn(nx+ny,N);

for k = 1:N
    % Get input for this timestep
    u = u_arr(:,k); 
    
    
    % Draw some noise
    noise = joint_noise_SD*randnstore(:,k);
    v = noise(1:nx);
    n = noise(nx+1:end);

    
    % Run SS
	y_arr(:,k) = C*x + D*u + n;  
    
    x = A*x + B*u + v;
    xtrue_arr(:,k+1) = x;
    
    
    

end