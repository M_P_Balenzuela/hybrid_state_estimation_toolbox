function [M_red,L_red,s_red,r_red] = KLR(L,s,r, epsilon,minPerCompatible)

        
    [nx,~,M] = size(L);

    L_red = nan(nx,nx,M);
    s_red = nan(nx,M);
    r_red = nan(M,1);
    
    M_red = 0;

    while M >0
        Li = L(:,:,1);
        si = s(:,1);
        ri = r(1);

        compIdx = false(M,1);
        compIdx(1) = true;
        for j = 2:M
            Lj = L(:,:,j);
            sj = s(:,j);
            rj = r(j);

            [~,tmp] =  GL_merge(Li,si,ri,Lj,sj,rj, epsilon);

            if ~isinf(tmp)
                compIdx(j) = true; 
            end    
        end

        [Mr, Lr,sr,rr] = KLR_reduce_compatible_likelihoods(L(:,:,compIdx),s(:,compIdx),r(compIdx), epsilon,minPerCompatible);
        
        idx = (M_red+1):(M_red+Mr);
        M_red= M_red + Mr;
        L_red(:,:,idx) = Lr;
        s_red(:,idx)= sr;
        r_red(idx) = rr;

        L = L(:,:,~compIdx);
        s = s(:,~compIdx);
        r = r(~compIdx);

        M = M-sum(real(compIdx));

    end

    idx = 1:M_red;
    L_red = L_red(:,:,idx);
    s_red = s_red(:,idx);
    r_red = r_red(idx);
    

end



function [M,L,s,r] = KLR_reduce_compatible_likelihoods(L,s,r, epsilon, minPerCompatible)

    M = size(L,3);
    while M > minPerCompatible
        [i,j] = find_merge_pair(L,s,r,epsilon);
        Li = L(:,:,i);
        si = s(:,i);
        ri = r(i);
        Lj = L(:,:,j);
        sj = s(:,j);
        rj = r(j);    

        [~,~,Lij,sij,rij] =  GL_merge(Li,si,ri,Lj,sj,rj, epsilon);
        
        L(:,:,i) = Lij;
        s(:,i) = sij;
        r(i) = rij;
        
        ej = true(M,1);
        ej(j) = false;
        L = L(:,:,ej);
        s = s(:,ej);
        r = r(ej);
        
        M = M-1;
    end

end


function [i,j] = find_merge_pair(L,s,r,epsilon)

    M = size(L,3);

    relB = inf(M,M);
    convto1 = ones(M,1);
    % only fill the UT of relB
    for j = 2:M
        Lj = L(:,:,j);
        sj = s(:,j);
        rj = r(j);
        for i = 1:j-1

            Li = L(:,:,i);
            si = s(:,i);
            ri = r(i);

            [wjmw1,relBij] = GL_merge(Li,si,ri,Lj,sj,rj, epsilon);
            relB(i,j) = relBij;
            if i == 1
                convto1(j) = wjmw1; %wj-w1
            end
        end
    end

    relB = repmat(convto1,1,M) .* relB;

    [i,j] = misc.minMat(relB);
    
    

end


function [ewjmwi,Bij_reli,Lij,sij,rij] =  GL_merge(Li,si,ri,Lj,sj,rj, epsilon)


[Uxi,Di] = svd(Li);
 ni = sum(Di(:) > epsilon);
 Ui = Uxi(:,1:ni);
 Zi = Uxi(:,ni+1:end);
 
[Uxj,Dj] = svd(Lj);
nj = sum(Dj(:) > epsilon);
Uj = Uxj(:,1:nj);
Zj = Uxj(:,nj+1:end);

% check for compatible range spaces
if (ni ~= nj) % range spaces can't possibly be equal with different rank
    Bij_reli = inf;
    return;
end

% Check null and range space basis have no overlap (dot product zero)
Ck1 = Ui.'*Zj;
Ck2 = Uj.'*Zi;

if any(abs(Ck1(:)) > epsilon) || any(abs(Ck2(:)) > epsilon)
    Bij_reli = inf;
    return;
end

Sigmai = Di(1:ni,1:ni);


Sigmaj = Ui.'*Lj(1:ni,1:ni)*Ui; % we already checked ni=nj

etai = Ui.'*si;
etaj = Ui.'*sj;

invSigmai = eye(ni) / Sigmai;
invSigmaj = eye(ni) / Sigmaj;

%ri and rj stuff
twoPi_ni = ni*log(2*pi);
logdetSigmai = log(det(Sigmai));
logdetSigmaj = log(det(Sigmaj));
ei = -0.5*(ri-etai.'/Sigmai*etai + logdetSigmai);
ej = -0.5*(rj-etaj.'/Sigmaj*etaj + logdetSigmaj);
ewjmwi = exp(ej-ei);
if nargout > 1
    ai = exp(ei + 0.5*twoPi_ni);
    aj = exp(ej + 0.5*twoPi_ni);

    vi = ai/(ai+aj);
    vj = aj/(ai+aj);
    mui = Sigmai\etai;
    muj = Sigmaj\etaj;
    invSigmaij = vi*invSigmai+vj*invSigmaj+vi*vj*(mui-muj)*(mui-muj).';
    logdetinvSigmaij = log(det(invSigmaij));
    Bij_reli = (ai+aj)*logdetinvSigmaij+ai*logdetSigmai+aj*logdetSigmaj;

    if nargout > 2
        etaij = invSigmaij\(vi*mui+vj*muj);
        rij = etaij.'*invSigmaij*etaij - 2*log(ai+aj) + twoPi_ni + logdetinvSigmaij;
        sij = Ui*etaij;
        Lij = Ui/invSigmaij*Ui.';
    end
end
end