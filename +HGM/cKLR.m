function [zred, lnwzred, muzred, cPzred] = cKLR(z,lnw, mu, cP, Mu, Ml, lambda)
% Reduce a Hybrid GM density down to one of fewer components using KL reduction

% IN:
    % z(i) - Discrete state of i-th mode
    % lnw(i) - Log-weight of i-th mode
    % mu(:,i) - Mean of i-th mode
    % cP(:,:,i) - Chol Covariance of i-th mode
    % Mu - Maximum number of modes per discrete variable in reduced mixture

% OPTIONAL INPUTS:
	% Ml - Minimum number of components after reduction per discrete state (may be fewer if input
	% GM has less components for that discrete state)
	% lambda - Error threshold, used for deturmining if mixture should have
	% fewer than Mu elements

% OUT:
	% nummodes - Number of components in reduced mixture
    % zred(i) - Discrete state of the i-th mode in the mixture
	% lnwzred(i) - Log-weight of i-th mode in reduced mixture
	% muzred(:,i) - Mean of i-th mode in reduced mixture
	% cPzred(:,:,i) - Chol Covariance of i-th mode in reduced mixture


if nargin < 7
    lambda = 0;
end
if nargin < 6
    Ml = 1;
end

% Get number of continious and discrete states
minz = min(z);
maxz = max(z);
nz = maxz-minz+1;

nx = size(mu,1);

% pre-allocate space
zred = nan(nz*Mu,1);
lnwzred = nan(nz*Mu,1);
muzred = nan(nx,nz*Mu);
cPzred = nan(nx,nx,nz*Mu);

nummodes = 0;
for zi = minz:maxz
    idxs = find(zi==z);
    
    lnwz = lnw(idxs);
    muz = mu(:,idxs);
    cPz = cP(:,:,idxs);

    [Mz, lnwz, muz, cPz] = HSET.GM.cKLR(lnwz, muz, cPz, Mu, Ml, lambda);

    zred(nummodes+1:nummodes+Mz) = zi;
    lnwzred(nummodes+1:nummodes+Mz) = lnwz;
    muzred(:,nummodes+1:nummodes+Mz) = muz;
    cPzred(:,:,nummodes+1:nummodes+Mz) = cPz;
    nummodes=nummodes+Mz;
end

zred = zred(1:nummodes);
lnwzred = lnwzred(1:nummodes);
muzred = muzred(:,1:nummodes);
cPzred = cPzred(:,:,1:nummodes);