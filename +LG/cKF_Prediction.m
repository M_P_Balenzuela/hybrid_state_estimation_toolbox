function [mup,cPp] = cKF_Prediction(muf,cPf,A,b,cQ)

persistent struct_UT_TRANSA 
if isempty(struct_UT_TRANSA) 
    struct_UT_TRANSA = struct('UT',true,'TRANSA',true);   
end

% get dimms
nx = size(A,1);

% Prediction step
  QR1 = [cQ;
        cPf*A.'];

% Q-less QR decomp.    
R1 = triu(qr(QR1));
cPp = R1(1:nx,1:nx); %R1_11 is sq of APA' + Q
mup = A*muf+b;

end




