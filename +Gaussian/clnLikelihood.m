function lnL = clnLikelihood(es,cP)
%Calculate the normal loglikelihood ln(N(x| mu,P))

persistent struct_UT_TRANSA 
if isempty(struct_UT_TRANSA) 
    struct_UT_TRANSA = struct('UT',true,'TRANSA',true);   
end

% IN:
    % cP - UT chol-Covariance for all samples
    % mu - mean of the normal distribution
    % x(:,i) - value of the random variable to be evaluated against the dist.

% OUT: 
    % lnL(i) - log-likelihood corresponding to point x(:,i)
    

nx = size(cP,1);

% Numerically terrible way
% L = -0.5*log(det(2*pi*P)) -diag(0.5*es'/P*es);

% Better Maths intensive way
%lnNc = TrLn(cP);
A = diag(cP);
A = abs(A);
A = log(A);
lnNc = sum(A);

con = log(2*pi);
con = nx*con;
con = 0.5*con;
lnNc = lnNc + con;

es = linsolve(cP,es, struct_UT_TRANSA);
es = es .* es;
lnL = sum(es,1);
lnL = lnL.';
lnL = 0.5*lnL;

lnL = lnNc + lnL;
lnL = -lnL;
