function rnd = cRand(mu,cP,n)
% Generate n random column vectors from distribution N(mu,P), where P = cP.'*cP
nx = size(mu,1);
% y=Ax, x~N(0,P), then y~N(0,APA.')
% let P=I & A=cP.' then y~N(0,cP.'cP) -> y~N(0,P)
rnd = mu + cP.'*randn(nx,n);