function [xtrue,ztrue,yidx] = simulate(x0,JMLS,uidx,z0)

% Grab dims.
[~,N] = size(uidx);
N = N-1;
[ny,nx] = size(JMLS.model(1).C);

% if z0 is a scaler, or if z0 is a discrete traj.
if isscalar(z0)
    ztrue = nan(N,1);
    z = z0;
    ztrue(1) = z;
    for k=2:N+1
        z = randsample(JMLS.N,1,true,exp(JMLS.lnT(:,z)) );
        ztrue(k) = z;
    end
else
    ztrue = z0;
end


xtrue = nan(nx,N+1);
yidx = nan(ny,N+1);

% Load initial condition
x = x0;
xtrue(:,1) = x0;

for k = 1:N
    u = uidx(:,k:k+1);
    
    z = ztrue(k+1);
    SS = JMLS.model(z);
    
    [x,y] = HSET.LG_SS.simulate(x,SS,u);
    x = x(:,2);
    xtrue(:,k+1) = x;
    yidx(:,k+1) = y(:,2);    
	
end