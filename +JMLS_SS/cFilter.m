function [lnw_f,mu_f,cP_f,z_f, LnL] = cFilter(lnwc,muc,cPc,zc,cJMLS,ym,u)

if nargin < 7
    u = zeros(0,1);
end

nz = cJMLS.N;
nx = size(cJMLS.model(1).A,1);
lnT = cJMLS.lnT;

Nf = length(lnwc);
z_f = nan(Nf,1);
lnw_f = nan(Nf,1);
mu_f = nan(nx,Nf);
cP_f = nan(nx,nx,Nf);


idx = 0;
for zi = 1:nz
    [A,b,cQ,C,d,cR] = HSET.LG_SS.cUnpack(cJMLS.model(zi),u);
    d_minus_ym = d - ym; 
    sidx = idx;
    for i = 1:Nf
        idx = idx+1;
        [mup,cPp] = HSET.LG.cKF_Prediction(muc(:,i),cPc(:,:,i),A,b,cQ);

        [mu_f(:,idx),cP_f(:,:,idx),tmp] = HSET.LG.cKF_Correction(mup,cPp,C,d_minus_ym,cR);
        tmp = tmp + lnT(zi,zc(i));
        lnw_f(idx) = tmp + lnwc(i);
    end
    z_f(sidx+1:idx) = zi;
end

if nargout > 4
    LnL = HSET.misc.LSE(lnw_f);
end

lnw_f = HSET.misc.normalise_lnw(lnw_f);
    