function [L_b,s_b,r_b,z_b,lnws,mus,Ps,zs] = jointTFS(JMLS,u,ym,Pf,muf,lnwf,zf,Lb,sb,rb,zb)

if (nargin > 7)
    [nx,Mbif] = size(sb);
else
    Mbif = 1;
    nx = size(JMLS.model(1).A,1);
end

Nf = length(lnwf);
nz = JMLS.N;
N_b = Mbif*nz;



L_b = nan(nx,nx,N_b);
s_b = nan(nx,N_b);
r_b = nan(N_b,1);
z_b = nan(N_b,1);

Lbc = nan(nx,nx,Mbif);
sbc = nan(nx,Mbif);
rbc = nan(Mbif,1);

Ns = N_b*Nf;
nxc = 2*nx;
Ps = nan(nxc,nxc,Ns);
mus = nan(nxc,Ns);
lnws = nan(Ns,1);
zs = nan(2,Ns);

sidx = 0;
bidx = 0;
for z = 1:nz    
    % Unpack model
    [A,b,Q,C,d,R] = HSET.LG_SS.unpack(JMLS.model(z),u); 
    for i = 1:Mbif
        bidx = bidx +1;
        
        if (nargin > 7)
            Lbi = Lb(:,:,i) + C'/R*C;
            sbi = sb(:,i) + C'/R*(d-ym);
            rbi = rb(i) + (d-ym)'/R*(d-ym) + log(det(2*pi*R))-2*JMLS.lnT(zb(i),z);
        else
            Lbi = C'/R*C;
            sbi = C'/R*(d-ym);
            rbi = (d-ym)'/R*(d-ym) + log(det(2*pi*R));
            
        end
        
        Lbc(:,:,i) = Lbi;
        sbc(:,i) = sbi;
        rbc(i) = rbi;
        
        I = eye(nx);
        Phi = (I+Lbi*Q)\Lbi;
        Psi = Q*Phi*Q-Q;
        Beta = I - Q*Phi;

        
        z_b(bidx) = z;
        r_b(bidx) = rbi - log(det(Beta)) + [sbi; b].'*[Psi Beta; Beta.' Phi]*[sbi; b];
        s_b(:,bidx) = A.'*(Phi*b + Beta.'*sbi);
        L_b(:,:,bidx) = A.'*Phi*A;
    end
    
    % compute joint [xk;xk+1]|Y
    for i = 1:Nf
        
        mufi = muf(:,i);
        Pfi = Pf(:,:,i);
        
        muji = [mufi;
        A*mufi+b];

        Pji = [Pfi, Pfi*A.';
        A*Pfi, A*Pfi*A.'+Q]; 
    
        [Ljp,sjp,rjp] = HSET.GM.to_GML(Pji,muji,lnwf(i));
        
        for j = 1:Mbif
            sidx = sidx +1;
            Lbj = Lbc(:,:,j);
            sbj = sbc(:,j);
            rbj = rbc(j);
            
            Ls = Ljp;
            Ls(nx+1:end,nx+1:end) = Ls(nx+1:end,nx+1:end) + Lbj;

            ss = sjp;
            ss(nx+1:end) = ss(nx+1:end) + sbj;

            rs = rjp + rbj -2*JMLS.lnT(z,zf(i));
            
            [Psijz,musijz,lnwsijz] = HSET.GML.to_GM(Ls,ss,rs);
            
            Ps(:,:,sidx) = Psijz;
            mus(:,sidx) = musijz;
            lnws(sidx) = lnwsijz;
            zs(:,sidx) = [zf(i);z];
        end
        
    end

end

r_b = HSET.misc.normalise_lnw(r_b);

lnws = HSET.misc.normalise_lnw(lnws);




