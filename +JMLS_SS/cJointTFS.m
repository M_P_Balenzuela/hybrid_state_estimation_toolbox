function [cL_b_arr,s_b_arr,r_b_arr,z_b_arr,lnws_arr,mus_arr,cPs_arr,zs_arr] = cJointTFS(cJMLS,u,ym,cPf_arr,muf_arr,lnwf_arr,zf_arr,  cLb_arr,sb_arr,rb_arr,zb_arr)

nx = size(cJMLS.model(1).A,1);
two_nx = 2*nx;
nz = cJMLS.N;
lnT = cJMLS.lnT;


if nargin < 8 % initialising
   Nb = 1;
   cLb_arr = zeros(nx,nx);
   sb_arr = zeros(nx,1);
   rb_arr = 0;
else
   Nb = length(rb_arr);
end    



N_b = Nb*nz;
cL_b_arr = nan(nx,nx,N_b);
s_b_arr = nan(nx,N_b);
r_b_arr = nan(N_b,1);
z_b_arr = nan(N_b,1);

Nf = length(lnwf_arr);
N_s = N_b*Nf;
cPs_arr = nan(two_nx,two_nx,N_s);
mus_arr = nan(two_nx,N_s);
lnws_arr = nan(N_s,1);
zs_arr = nan(2,N_s);


idx = 0;
smoo_idx = 0;
for zi = 1: nz % discrete switching
        [A,b,cQ,C,d,cR] = HSET.LG_SS.cUnpack(cJMLS.model(zi),u);
        d_minus_ym = d - ym;
        sidx = idx;
        for i = 1:Nb
            idx = idx + 1;

            [cL_c,s_c,r_c] = HSET.LG.cBIF_Correction(cLb_arr(:,:,i),sb_arr(:,i),rb_arr(i),C,d_minus_ym, cR);
            if ~(nargin < 8)
                r_c = r_c - 2*lnT(zb_arr(i),zi);
            end
            
            for f = 1:Nf
                smoo_idx = smoo_idx+1;
                cPf = cPf_arr(:,:,f);
                muf = muf_arr(:,f);
                lnwf = lnwf_arr(f);
                zff = zf_arr(f);
                [cPs_arr(:,:,smoo_idx),mus_arr(:,smoo_idx),lnws_arr(smoo_idx)] = HSET.LG.cTFjoint_former(cPf, muf,lnwf, cL_c,s_c,r_c, A,b,cQ,lnT(zi,zff));
                zs_arr(:,smoo_idx) = [zff;zi];
            end
            
            [cL_b_arr(:,:,idx),s_b_arr(:,idx),r_b_arr(idx)] = HSET.LG.cBIF_BackPropagation(cL_c,s_c,r_c,A,b,cQ);

        end
        z_b_arr(sidx+1:idx) = zi;
end

r_b_arr = HSET.misc.normalise_lnw(r_b_arr);

lnws_arr = HSET.misc.normalise_lnw(lnws_arr);

