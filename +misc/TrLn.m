function out = TrLn(P)
% Take the sum of the log of the diagnonal elements in P
% This is equivilant to LnDet(P) if P is a UT matrix
% If P=chol(A), then log(det(A)) = 2*TrLn(P)

A = diag(P);
A = abs(A);
A = log(A);
out = sum(A);