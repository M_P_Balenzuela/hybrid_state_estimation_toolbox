function [Q,R] = my_qr(A,epso)

[Q,R] = qr(A);
vec = abs(diag(R)) > epso;
R = [R(vec,:);R(~vec,:)];
R = [R(:,vec) R(:,~vec)];

Q = [Q(:,vec) Q(:,~vec)];

