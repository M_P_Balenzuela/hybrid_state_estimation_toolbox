function C = chol_SPD(A)
% Cholesky Decomp. for SPD matricix A.

[L,D] = ldl(A);

C = (D.^0.5) * L.';