function A = rnd_P(nx)
% Generate a random, symetric positive definite matrix A with size nx*nx

A = iwishrnd(eye(nx),nx+1);