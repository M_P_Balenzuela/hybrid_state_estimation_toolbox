function lnwn = normalise_lnw(lnw)
% Vectorwise normalisation of log weights lnw, normalised along first
% dimension (each column)

lnwmax = max(lnw);
idx = (lnwmax == -inf);
lnw(:,idx) = 0;
lnwmax(idx) = 0;

lnwn = lnw-lnwmax-log(sum(exp(lnw-lnwmax)));