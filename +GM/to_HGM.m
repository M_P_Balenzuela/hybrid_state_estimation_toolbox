function [M,z,lnw,mu,P] = to_HGM(nz,lnw,mu,P)
% Generate coresponding GM prior for a JMLS estimator

% IN:
    % nz - Number of discrete states
    % lnw(i) - Log-weight of i-th mode in GM prior
    % mu(:,i) - Mean of i-th mode in GM prior
    % P(:,:,i) - Covariance of i-th mode in GM prior
    
% OUT:
    % M - Number of modes in JMLS prior
    % d(i) - Discrete state of i-th mode in JMLS prior
    % lnw(i) - Log-weight of i-th mode in JMLS prior
    % mu(:,i) - Mean of i-th mode in JMLS prior
    % P(:,:,i) - Covariance of i-th mode in JMLS prior
    
    
%  Get dims.
M = length(lnw);

% Weights
lnw = repmat(lnw,nz,1);
lnw = HSET.misc.normalise_lnw(lnw);

% Means
mu = repmat(mu,1,nz);

% Covariances
P = repmat(P,1,1,nz);

% Discrete state, d = [1, ..., 1, 2, ..., nz, nz].';
z = 1:(M*nz);
z = z.'-1;
z = 1 + floor(z/M);